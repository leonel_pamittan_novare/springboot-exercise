package bootcamp.exercise.controller;

import bootcamp.exercise.entity.SendMail;
import bootcamp.exercise.service.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class SendMailController {

    @Autowired
    SendMailService sendMailService;

    @PostMapping
    public ResponseEntity<String> add(@RequestBody SendMail sendMail)
    {
        sendMailService.toSendMail(sendMail);
        ResponseEntity<String> responseEntity = new ResponseEntity("It works!!", HttpStatus.CREATED);
        return responseEntity;
    }
}
