package bootcamp.exercise.service;

import bootcamp.exercise.entity.SendMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.Properties;

@Service
public class SendMailServiceImpl implements SendMailService{

    @Autowired
    public JavaMailSender javaMailSender;

    @Override
    public void toSendMail(SendMail sendMail){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(sendMail.getEmail());
        message.setSubject(sendMail.getSubject());
        message.setText(sendMail.getBody());
        javaMailSender.send(message);
    }

    @Bean
    public JavaMailSender getJavaMailSender(){
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost("smtp.gmail.com");
        javaMailSender.setPort(587);

        javaMailSender.setUsername("rj.vocalan03@gmail.com");
        javaMailSender.setPassword("Rjvocalan02!");

        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.transport.protocol","smtp");
        props.put("mail.smtp.auth","true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("","");
        props.put("mail.debug", "true");

        return  javaMailSender;
    }

}
